﻿/*
 * microp11 2017
 * 
 * This file is part of Scytale-C.
 * 
 * Scytale-C is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Scytale-C is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Scytale-C.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * With help from Jonti:
 * https://github.com/jontio/JDSCA/blob/73ef28e971203b164706dc7ffcc7c941a3b38f5a/JDSCA/DSP.h
 * 
 */

using System;
using System.Diagnostics;
using System.Numerics;

namespace ScytaleC
{
    public class Gardner
    {
        private double ts;
        private double symbol2xTimer;
        private int samplesAgo;
        private Complex maxErrorxAggression;
        private int isOnPoint;
        private Complex currentSample;
        private Complex sampleThisOn;
        private Complex error;
        private Complex sampleLastOn;
        private Complex sampleOff;
        private double aggression;
        private bool result;
        private Complex dummy_output;

        public Gardner(double sampleRate, double symbolRate)
        {
            ts = sampleRate / symbolRate;
            symbol2xTimer = 0;
            samplesAgo = 0;
            maxErrorxAggression = ts / 4;
            isOnPoint = 1;
            sampleLastOn = new Complex(0, 0);
            aggression = 0.1;
            dummy_output = new Complex(0, 0);
        }

        public bool Step(double Re, double Im, out Complex output, out bool onPoint)
        {
            //default return and output
            result = false;
            output = dummy_output;
            onPoint = false;

            // these are samples after the carrier tracking and the RRC filter
            // current_sample = data_re(i) + 1i* data_im(i);
            currentSample = new Complex(Re, Im);

            // sample at 2x bit rate
            symbol2xTimer += 2;

            // simple way to avoid double sampling due to timing jitter
            samplesAgo++;

            if (symbol2xTimer >= ts && ComplexMath.BiggerThan(samplesAgo, Complex.Add(maxErrorxAggression, 1)))
            {
                symbol2xTimer -= ts;
                samplesAgo = 0;
                isOnPoint = 1 - isOnPoint;

                // on time here
                if (isOnPoint == 1)
                {
                    sampleThisOn = currentSample;

                    // calculate carrier timing error
                    error = Complex.Multiply(Complex.Subtract(sampleThisOn, sampleLastOn), sampleOff);

                    // steer symbol timing
                    symbol2xTimer = symbol2xTimer + Complex.Multiply(error, aggression).Real;

                    // save on symbol
                    result = true;
                    output = sampleThisOn;
                    onPoint = true;

                    // save this on time for next on time
                    sampleLastOn = sampleThisOn;
                }

                // we are off time here
                else
                {
                    sampleOff = currentSample;

                    // save off symbol
                    // we can control here how ofter we want the CMA to update its filter coefs
                    // if we set it to true, CMA will go for every sample not on point
                    result = true;
                    output = sampleOff;
                    onPoint = false;
                }
            }

            return result;
        }
    }

    public class CMA
    {
        public bool result;
        private Complex dummy_output;
        private int cma_sz;
        private Complex[] cma_w;
        private Complex[] cma_x;
        double beta;
        private double stepsize;
        private double mean;
        private Complex cmaEqualizerOut;
        private Complex error;

        public CMA()
        {
            dummy_output = new Complex(0, 0);
            
            // no of points of the fir filter
            cma_sz = 9;

            cma_w = new Complex[cma_sz];
            for (int i = 0; i < cma_sz; i++)
            {
                cma_w[i] = new Complex(0, 0);
            }
            //cma_w((cma_sz+1)/2)=1.0+1i*0; verify
            cma_w[(cma_sz) / 2] = new Complex(1, 0);

            cma_x = new Complex[cma_sz];
            for (int i = 0; i < cma_sz; i++)
            {
                cma_x[i] = new Complex(0, 0);
            }

            // there is some stats with this number and depends on what agc is set to
            beta = Math.Sqrt(2);

            // this makes a huge difference
            //stepsize = 0.001;
            stepsize = 0.001;
        }

        public bool Step(Complex sample, bool isOnPoint, out Complex output)
        {
            //default return and output
            output = dummy_output;
            result = false;

            // load another sample these samples are running at 2x speed
            // but could be running faster if you want and i think would produce a
            // better equalizer but uses more cpu

            if (isOnPoint)
            {
                //AGC
                mean = (Complex.Abs(sample) + Complex.Abs(output)) / 2;
                sample = Complex.Divide(sample, new Complex(mean, 0));
            }

            // run samples though the equalizer
            // how often this runs is not critical but it should be constant wrt
            // symbol timing
            for (int i = 0; i < cma_sz - 1; i++)
            {
                cma_x[i] = cma_x[i + 1];
            }
            cma_x[cma_sz - 1] = sample;

            cmaEqualizerOut = new Complex(0, 0);
            for (int i = 0; i < cma_sz; i++)
            {
                cmaEqualizerOut = Complex.Add(cmaEqualizerOut, Complex.Multiply(cma_w[i], cma_x[i]));
            }

            // this must be run only when you think cma_equalizer_out is an on point
            // if on time then update cma algo equalizer coeffs
            if (isOnPoint)
            {

                // save samples that come out of the equalizer that are on point
                output = cmaEqualizerOut;
                result = true;

                // calc directional error. this one here only does modulus not
                // rotation.the rotation one is in jdsca but is still simple
                // error = cma_equalizer_out * ((abs(cma_equalizer_out)) ^ 2 - beta); verify
                error = Complex.Multiply(cmaEqualizerOut, new Complex(Math.Pow(Complex.Abs(cmaEqualizerOut), 2) - beta, 0));

                // step filer coeffs in direction opposite direction of error
                for (int i = 0; i < cma_sz; i++)
                {
                    Complex right = Complex.Conjugate(cma_x[i]);
                    right = Complex.Multiply(error, right);
                    right = Complex.Multiply(new Complex(stepsize, 0), right);
                    cma_w[i] = Complex.Subtract(cma_w[i], right);
                    //if (cma_w[i].Magnit > 10)
                }

            }

            return result;
        }

        public void CmaReset()
        {
            cma_w = new Complex[cma_sz];
            for (int i = 0; i < cma_sz; i++)
            {
                cma_w[i] = new Complex(0, 0);
            }
            //cma_w((cma_sz+1)/2)=1.0+1i*0; verify
            cma_w[(cma_sz) / 2] = new Complex(1, 0);

            cma_x = new Complex[cma_sz];
            for (int i = 0; i < cma_sz; i++)
            {
                cma_x[i] = new Complex(0, 0);
            }
        }
    }

    internal class ComplexMath
    { 
        /// <summary>
        /// Estimates distance from origin, the closer, the "smaller" 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool LessThan(Complex left, Complex right)
        {
            return Math.Sqrt(left.Real * left.Real + left.Imaginary * left.Imaginary) < Math.Sqrt(right.Real * right.Real + right.Imaginary * right.Imaginary);
        }

        /// <summary>
        /// Estimates distance from origin, the further, the "bigger" 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool BiggerThan(Complex left, Complex right)
        {
            return LessThan(right, left);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AGC
    {
        private double mAGC;

        public AGC()
        {
            mAGC = 0;
        }

        public void Apply(ref double I, ref double Q)
        {
            double magnitude = Math.Sqrt(I * I + Q * Q);
            if (magnitude > mAGC)
            {
                mAGC = (1 - 1 / 250) * mAGC + (1 / 250) * magnitude;
            }
            else
            {
                mAGC = (1 - 1 / 1000) * mAGC + (1 / 1000) * magnitude;
            }

            if (mAGC >= 1)
            {
                I = I / mAGC;
                Q = Q / mAGC;
            }
        }

        public void Apply(ref Complex value)
        {
            double magnitude = Math.Sqrt(value.Real * value.Real + value.Imaginary * value.Imaginary);
            if (magnitude > mAGC)
            {
                mAGC = (1 - 1 / 250) * mAGC + (1 / 250) * magnitude;
            }
            else
            {
                mAGC = (1 - 1 / 1000) * mAGC + (1 / 1000) * magnitude;
            }

            if (mAGC >= 1)
            {
                value = new Complex(value.Real / mAGC, value.Imaginary / mAGC);
            }
        }

        public void Reset()
        {
            mAGC = 0;
        }
    }
}
