﻿/*
 * microp11 2017
 * 
 * Sources: file or stream.
 * The decoding chain supports with minimum changes real IQ input and that would be the next step.
 * 
 * The data is stored into the sourceQue. It is consumed by the decoder and the fft display.
 * The decoder data output is stored into the decoderOutputQue. The decoder also has the capability of raising
 * events. This are synchronous events Windows style. If you want to use them, make sure your code does not
 * slow the decoding down. I would suggest less then 50ms per handler.
 * The decoder signal output goes into its separate queueu as it has a different speed than the data output.
 * The decoder data and signal outputs are consumed by the UI for display purposes.
 * 
 * TODO:
 *  Write a proper producer/consumer using Tasks and async and get rid of timers.
 *  Check the dynamic loading of the assemblies...
 *  Unify the sources under one common interface.
 *  
 *  https://stackoverflow.com/questions/15238714/net-local-assembly-load-failed-with-cas-policy
 *  https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?&page=121
 *  https://stackoverflow.com/questions/2920696/how-generate-unique-integers-based-on-guids
 *  http://www.java2s.com/Code/CSharp/Network/TcpClientSample.htm
 *  
 */

using NAudio.Wave;
using ScytaleC.Decoder;
using ScytaleC.PacketDecoders;
using ScytaleC.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Net.Sockets;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using System.Windows.Forms;

namespace ScytaleC
{
    public partial class MainForm : Form
    {
        private WaveIn waveIn;
        private NetworkStream tcpIpStream;
        private WaveFormat tcpIpWaveFormat;
        private TcpClient tcpClient;
        private byte[] tcpData;
        private StreamQueue sourceQue = new StreamQueue();
        private WaveFileReader wavFile;
        private byte[] wavData;
        private bool continueToReadFile;
        private bool continueToReadTcp;
        private bool isFileInput;
        private bool isTcpIpInput;
        private WavReaderState readStateObj = null;
        private string exePath;
        private static Exception InterfaceNotFoundException;
        private int framesReceived = 0;
        private uint symbols = 0;

        int testCounter = 0;
        private bool consumingAllowed;
        private Demodulator demodulator;
        private Decoder.Decoder decoder;
        private Stopwatch sw;
        private BufferBlock<AudioSampleArgs> sampleBufferQue;
        private BufferBlock<DemodulatedSymbolsArgs> decoderOutputQue;
        private BufferBlock<SignalAttributesArgs> signalAttributesQue;

        private BufferBlock<byte[]> decoderInput;
        private BufferBlock<byte[]> decoderOutput;

        private int udpFrameCount = 0;
        private UdpTx udpr;

        private Statistics stats;
        private long StreamId;
        private int selectedChannelndex = 0;

        public MainForm()
        {
            InitializeComponent();
            rtbInfo.Text = string.Format("Scytalec-C, version {0}{1}{1}", Assembly.GetExecutingAssembly().GetName().Version.ToString(), Environment.NewLine);
            InterfaceNotFoundException = new Exception("Interface not found!");

            exePath = AppDomain.CurrentDomain.BaseDirectory;
            FileOutput.SelectedPath = exePath;
            RSource_CheckedChanged(null, null);

            InitializePlaybackDevices();
            InitializeRemainingVisualElements();

            sampleBufferQue = new BufferBlock<AudioSampleArgs>();
            decoderOutputQue = new BufferBlock<DemodulatedSymbolsArgs>();
            signalAttributesQue = new BufferBlock<SignalAttributesArgs>();

            demodulator = new Demodulator();
            demodulator.SetCenterFrequency(DataConsts.InmarsatCCenterFrequency);
            demodulator.CmaEnabled = Settings.Default.cbCMA;
            demodulator.AgcEnabled = Settings.Default.cbAGC;
            demodulator.SetLoFreq((int)Settings.Default.nLoFreq);
            demodulator.SetHiFreq((int)Settings.Default.nHiFreq);

            demodulator.Demodulate(sampleBufferQue, decoderOutputQue, signalAttributesQue);
            fftD.SetDemodulator(demodulator);

            decoderInput = new BufferBlock<byte[]>();
            decoderOutput = new BufferBlock<byte[]>();
            decoder = new Decoder.Decoder((int)nTolerance.Value, decoderInput, decoderOutput);
            decoder.OnDemodulatedSymbols += ScyDecoder_OnDemodulatedSymbols;

            sw = new Stopwatch();
            stats = new Statistics();
            stats.OnStatsUpdate += Stats_OnStatsUpdate;

            //Set a new stream id every time we restart the decoder.
            DateTime now = DateTime.UtcNow;
            StreamId = (now.Ticks / 10000);
            string sl = StreamId.ToString();
            lblStreamId.Text = string.Format("SId: *{0,5}", sl.Substring(sl.Length - 5));

            //tcp input
            tcpIpStream = null;
            tcpClient = null;

            ConsumeSignalAttributes();
            ConsumeDemodulatedSymbols();
            ConsumeDescrambledFrames();
        }

        private void ScyDecoder_OnDemodulatedSymbols(object sender, Decoder.DemodulatedSymbolsArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                symbols += (uint)e.Length;

                lblRxSYM.Text = string.Format("Rx SYM: {0:0.00}M", symbols / 1.0e6);

                if (cbSuspendSleepMode.Checked)
                {
                    NativeMethods.SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED);
                }
            }));
        }

        private void InitializeRemainingVisualElements()
        {
            rAudioDevice.Checked = Settings.Default.rAudioDevice;
            rFile.Checked = Settings.Default.rFile;
            rTcpIp.Checked = Settings.Default.rTcpIp;

            comboPlaybackChannels.SelectedIndex = Settings.Default.comboPlaybackChannelIndex;

            int selectedIndex = Settings.Default.comboPlaybackDevices;
            if (comboPlaybackDevices.Items.Count > selectedIndex)
            {
                comboPlaybackDevices.SelectedIndex = selectedIndex;
            }

            txtWaveFileInput.Text = Settings.Default.txtWaveFileInput;
            openWavFileInput.FileName = Settings.Default.openWavFileInput;

            txtUdpAddress.Text = Settings.Default.txtUdpAddress;
            nTcpPort.Value = Settings.Default.nTcpPort;

            cbCMA.Checked = Settings.Default.cbCMA;
            cbAGC.Checked = Settings.Default.cbAGC;
            nLoFreq.Value = Settings.Default.nLoFreq;
            NLoFreq_ValueChanged(this, null);
            nHiFreq.Value = Settings.Default.nHiFreq;
            NHiFreq_ValueChanged(this, null);

            nTolerance.Value = Settings.Default.nTolerance;

            nUdpPort.Value = Settings.Default.nUdpPort;
            cbUdpTransmit.Checked = Settings.Default.cbUdpTransmit;

            cbSuspendSleepMode.Checked = Settings.Default.cbSuspendSleepMode;

            if (rAudioDevice.Checked)
            {
                ActiveControl = rAudioDevice;
            }
            else if (rFile.Checked)
            {
                ActiveControl = rFile;
            }
            else
            {
                ActiveControl = rTcpIp;
            }

            nMaxDisplayedFrames.Value = Settings.Default.nMaxDisplayedFrames;
        }

        private void Stats_OnStatsUpdate(object sender, StatsArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                lblRxFR.Text = string.Format("Rx FR: {0}", e.RxFRCount);
                lblLostFR.Text = string.Format("Lost FR: {0}", e.LostFRCount);
                lblBBER.Text = string.Format("BBER: {0}%", (int)e.BBERPercent);
            }));
        }

        private void PresentFrame(DescrambledFrameArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                try
                {
                    string packetHex = Utils.BytesToHexString(e.DescrambledFrame, 0, e.Length);
                    int frameNumber = e.DescrambledFrame[2] << 8 | e.DescrambledFrame[3];

                    stats.AddRxFR(frameNumber, false);

                    if (frameNumber < 0 || frameNumber > 9999)
                    {
                        //bad frame
                        rtbFrames.SelectionBackColor = Color.PaleVioletRed;
                        rtbFrames.SelectionColor = Color.WhiteSmoke;
                        rtbFrames.AppendText(string.Format(" NOK {0:0.00} {1}", sw.ElapsedMilliseconds / 1000.0, packetHex));

                        rtbFrames.SelectionBackColor = Color.White;
                        rtbFrames.SelectionColor = Color.Black;
                    }
                    else
                    {
                        //possibly good frame
                        rtbFrames.SelectionBackColor = Color.PaleGreen;
                        rtbFrames.SelectionColor = Color.DarkBlue;
                        rtbFrames.AppendText(string.Format("{0}", frameNumber));

                        rtbFrames.SelectionBackColor = Color.White;
                        rtbFrames.SelectionColor = Color.Black;
                        rtbFrames.AppendText(string.Format(" {0:0.00} {1}", sw.ElapsedMilliseconds / 1000.0, packetHex));
                    }
                    sw.Restart();
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);
                }

                #region RestrictContentSizeAndScrolling

                int maxLinesOnDisplay = (int)nMaxDisplayedFrames.Value;
                if (rtbFrames.Lines.Length > maxLinesOnDisplay)
                {
                    rtbFrames.SelectionStart = rtbFrames.GetFirstCharIndexFromLine(0);
                    rtbFrames.SelectionLength = rtbFrames.Lines[rtbFrames.Lines.Length - maxLinesOnDisplay].Length + 1;
                    rtbFrames.SelectedText = String.Empty;
                }

                if (ActiveControl == rtbFrames)
                {
                    SendKeys.Send("+{TAB}");
                }

                rtbFrames.AppendText(Environment.NewLine);

                //scroll to last
                if (btnFramesAutoScroll.Checked)
                {
                    rtbFrames.ScrollToCaret();
                }

                #endregion
            }));
        }

        private void OnDescrambledFrame(object sender, DescrambledFrameArgs e)
        {
            if (cbUdpTransmit.Checked)
            {
                try
                {
                    //Append the StreamId to the frame
                    byte[] datagram = new byte[e.Length + 8];
                    Array.Copy(e.DescrambledFrame, datagram, e.Length);
                    byte[] sid = BitConverter.GetBytes(StreamId);
                    Array.Copy(sid, 0, datagram, e.Length, 8);

                    udpr?.Send(txtUdpAddress.Text, (int)nUdpPort.Value, Utils.BytesToHexString(datagram, 0, datagram.Length));
                    udpFrameCount++;
                    BeginInvoke(new Action(() =>
                    {
                        lblTxUDP.Text = string.Format("Tx UDP: {0}", udpFrameCount);
                    }));
                }
                catch (Exception ex)
                {
                    Utils.Log.Error(ex);
                }
            }

            PresentFrame(e);
        }

        /// <summary>
        /// https://stackoverflow.com/questions/22054414/update-ui-from-an-async-method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUWFinderFrame(object sender, UWFinderFrameArgs e)
        {
            BeginInvoke(new Action(() =>
            {
                framesReceived++;

                string s = (e.IsReversedPolarity) ? "I" : "N";
                s += (e.IsUncertain) ? "U" : "-";
                s += (e.IsMidStreamReversePolarity) ? "M" : "-";

                string s1 = string.Format("{0:00000} {1} {2:#0 }", e.SymbolCount, s, e.BER);
                rtbFrames.AppendText(s1);
            }));
        }

        private void OnSignalAttributes(object sender, SignalAttributesArgs e)
        {
            if ((int)e.Frequency == (int)nLoFreq.Value)
            {
                demodulator?.SetCenterFrequency((double)nHiFreq.Value);
                return;
            }

            try
            {
                BeginInvoke(new Action(() =>
                {
                    fftD.SetSignalAttributes(e);
                    lblSyncERR.Text = string.Format("Sync ERR: {0}", e.SyncCounter);

                    if (e.MeanMagnitude < 70)
                    {
                        if (!CmaResetTimer.Enabled)
                        {
                            CmaResetTimer.Enabled = true;
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                Utils.Log.ConditionalDebug(ex);
            }
        }

        private void StopConsuming()
        {
            consumingAllowed = false;
        }

        private async void StartConsuming()
        {
            //this logic needs to be changed
            bool success = false;

            consumingAllowed = true;
            while (consumingAllowed)
            {
                await Task.Delay(1);

                // for debugging
                if (sourceQue.Count > 100 && !isFileInput)
                {
                    Debug.WriteLine("que.Count = {0}", sourceQue.Count);
                }

                AudioSampleArgs asa = new AudioSampleArgs();

                success = sourceQue.TryTake(out Complex[] samples);
                if (!success)
                {
                    continue;
                }

                asa.Samples = samples;
                int length = asa.Samples.Length;
                asa.Length = length;

                for (int index = 0; index < length; index++)
                {
                    fftD.sampleAggregator.Add((float)asa.Samples[index].Real);
                }

                sampleBufferQue.Post(asa);
            }
        }

        private void InitializePlaybackDevices()
        {
            try
            {
                waveIn = new WaveIn();
                waveIn.RecordingStopped += WaveIn_RecordingStopped;

                Dictionary<int, string> selection = new Dictionary<int, string>();

                int waveInDevices = WaveIn.DeviceCount;
                for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
                {
                    WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                    selection.Add(waveInDevice, deviceInfo.ProductName);
                }

                comboPlaybackDevices.DataSource = new BindingSource(selection, null);
                comboPlaybackDevices.ValueMember = "Key";
                comboPlaybackDevices.DisplayMember = "Value";
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                MessageBox.Show("No sound system detected.");
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if ((string)btnStart.Tag == "start")
                {
                    Start();
                }
                else
                {
                    Stop();
                    RSource_CheckedChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
            }
        }

        private void SetButtons(int action)
        {
            switch (action)
            {
                case 0:
                    btnStart.Image = Resources.button_blue_stop;
                    EnableDisableControls(false);
                    btnStart.Tag = "stop";
                    break;

                case 1:
                    btnStart.Image = Resources.button_blue_play;
                    EnableDisableControls(true);
                    btnStart.Tag = "start";
                    break;

                case 2:
                    btnMenu.Image = Resources.exchange;
                    btnMenu.Tag = "menu-open";
                    break;

                case 3:
                    btnMenu.Image = Resources.exchange_back;
                    btnMenu.Tag = "menu-close";
                    break;
            }
        }

        private void EnableDisableControls(bool enabled)
        {
            rAudioDevice.Enabled = enabled;
            comboPlaybackDevices.Enabled = enabled;

            rFile.Enabled = enabled;
            txtWaveFileInput.Enabled = enabled;
            btnSelectWav.Enabled = enabled;

            rTcpIp.Enabled = enabled;
            txtTcpAddress.Enabled = enabled;
            nTcpPort.Enabled = enabled;

            comboPlaybackChannels.Enabled = enabled;
        }

        private void Stop()
        {
            SetButtons(1);
            stats.Stop();

            if (isFileInput)
            {
                continueToReadFile = false;
            }
            else if (isTcpIpInput)
            {
                continueToReadTcp = false;
            }
            else
            {
                if (readStateObj != null)
                {
                    readStateObj.ManualEvent.Set();
                    readStateObj = null;
                }
                waveIn.StopRecording();

                framesReceived = 0;
            }
            StopConsuming();
        }

        private async void Start()
        {
            // clear queue
            while (sourceQue.Count > 0)
            {
                sourceQue.TryTake(out Complex[] samplesToClear);
            }

            selectedChannelndex = comboPlaybackChannels.SelectedIndex;
            SetButtons(0);
            stats.Restart();

            udpr = new UdpTx();

            isFileInput = rFile.Checked;
            isTcpIpInput = rTcpIp.Checked;
            try
            {
                if (isFileInput)
                {
                    continueToReadFile = true;

                    if (wavFile != null)
                    {
                        wavFile.Dispose();
                    }
                    wavFile = new WaveFileReader(txtWaveFileInput.Text);
                    wavData = new byte[DataConsts.SamplesPerRead];

                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out Complex[] samplesToClear);
                    }
                    sourceQue.SetInputDataType(wavFile.WaveFormat);
                    wavFile.BeginRead(wavData, 0, DataConsts.SamplesPerRead, OnReadAsyncComplete, null);

                    await Task.Run(() => StartConsuming());
                }
                else if (isTcpIpInput)
                {
                    if (tcpIpStream != null)
                    {
                        tcpIpStream.Dispose();
                        tcpIpStream = null;
                    }

                    if (tcpClient != null)
                    {
                        tcpClient.Close();
                        tcpClient = null;
                    }

                    int noChannels = (selectedChannelndex > 0) ? 2 : 1;
                    tcpIpWaveFormat = new WaveFormat(DataConsts.AudioSampleRate, DataConsts.AudioBitsPerSample, noChannels);
                    tcpData = new byte[DataConsts.SamplesPerRead];

                    tcpClient = new TcpClient(txtTcpAddress.Text, (int)nTcpPort.Value);
                    continueToReadTcp = true;

                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out Complex[] samplesToClear);
                    }

                    sourceQue.SetInputDataType(tcpIpWaveFormat);
                    tcpIpStream = tcpClient.GetStream();
                    tcpIpStream.BeginRead(tcpData, 0, DataConsts.SamplesPerRead, OnTcpIpReadAsyncComplete, null);

                    await Task.Run(() => StartConsuming());
                }
                else
                {
                    if (waveIn != null)
                    {
                        waveIn.DataAvailable -= WaveIn_DataAvailable;
                        waveIn.RecordingStopped -= WaveIn_RecordingStopped;
                        waveIn.Dispose();
                        waveIn = new WaveIn();
                    }

                    waveIn.DeviceNumber = ((KeyValuePair<int, string>)comboPlaybackDevices.SelectedItem).Key;
                    waveIn.DataAvailable -= WaveIn_DataAvailable;
                    waveIn.RecordingStopped -= WaveIn_RecordingStopped;
                    waveIn.DataAvailable += WaveIn_DataAvailable;
                    waveIn.RecordingStopped += WaveIn_RecordingStopped;

                    int noChannels = (selectedChannelndex > 0) ? 2 : 1;
                    waveIn.WaveFormat = new WaveFormat(DataConsts.AudioSampleRate, DataConsts.AudioBitsPerSample, 2);
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out Complex[] samplesToClear);
                    }
                    sourceQue.SetInputDataType(waveIn.WaveFormat);
                    waveIn.StartRecording();

                    await Task.Run(() => StartConsuming());
                }

                CmaResetTimer.Enabled = true;
            }
            catch (Exception ex)
            {
                Utils.Log.Error(ex);
                Stop();
            }
        }

        private void OnTcpIpReadAsyncComplete(IAsyncResult ar)
        {
            int bytesRead;
            try
            {
                bytesRead = tcpIpStream.EndRead(ar);
                byte[] data = new byte[bytesRead];
                Buffer.BlockCopy(tcpData, 0, data, 0, bytesRead);
                //tcpip in
                sourceQue.Store(data, bytesRead, testCounter++, selectedChannelndex);
                //Debug.WriteLine("bytes read {0}", bytesRead);
            }
            catch
            {
                bytesRead = 0;
            }

            if (!continueToReadTcp || bytesRead == 0)
            {
                if (bytesRead == 0)
                {
                    //eof reached
                    SpinWait.SpinUntil(() => sourceQue.Count == 0 || !continueToReadTcp);

                    if (continueToReadTcp)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            BtnStart_Click(null, null);
                        });
                    }
                }
                else
                {
                    //the user has pressed the stop button
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out Complex[] samplesToClear);
                    }
                }
            }
            else
            {
                tcpData = new byte[DataConsts.SamplesPerRead];
                tcpIpStream.BeginRead(tcpData, 0, DataConsts.SamplesPerRead, OnTcpIpReadAsyncComplete, null);
            }
        }

        private void OnReadAsyncComplete(IAsyncResult ar)
        {
            int bytesRead;
            try
            {
                bytesRead = wavFile.EndRead(ar);
                byte[] data = new byte[bytesRead];
                Buffer.BlockCopy(wavData, 0, data, 0, bytesRead);
                //file in
                sourceQue.Store(data, bytesRead, testCounter++, selectedChannelndex);
            }
            catch
            {
                bytesRead = 0;
            }

            if (!continueToReadFile || bytesRead == 0)
            {
                if (wavFile != null)
                {
                    wavFile.Dispose();
                }
                if (bytesRead == 0)
                {
                    //eof reached
                    SpinWait.SpinUntil(() => sourceQue.Count == 0 || !continueToReadFile);

                    if (continueToReadFile)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            BtnStart_Click(null, null);
                        });
                    }
                }
                else
                {
                    //the user has pressed the stop button
                    // clear queue
                    while (sourceQue.Count > 0)
                    {
                        sourceQue.TryTake(out Complex[] samplesToClear);
                    }
                }
            }
            else
            {
                wavData = new byte[DataConsts.SamplesPerRead];
                wavFile.BeginRead(wavData, 0, DataConsts.SamplesPerRead, OnReadAsyncComplete, null);
            }
        }

        private void WaveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            int bytesRead = e.Buffer.Length;
            byte[] data = new byte[bytesRead];
            Buffer.BlockCopy(e.Buffer, 0, data, 0, bytesRead);
            //wave source
            sourceQue.Store(data, bytesRead, testCounter++, selectedChannelndex);
        }

        private void WaveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {
            StopConsuming();

            if (waveIn != null)
            {
                waveIn.DataAvailable -= WaveIn_DataAvailable;
                waveIn.RecordingStopped -= WaveIn_RecordingStopped;
            }
        }

        private void ReadUISettings()
        {
            isFileInput = rFile.Checked;
            isTcpIpInput = rTcpIp.Checked;
        }

        private void BtnSelectWav_Click(object sender, EventArgs e)
        {
            if (openWavFileInput.ShowDialog() == DialogResult.OK)
            {
                txtWaveFileInput.Text = openWavFileInput.FileName;
            }
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if ((string)btnMenu.Tag == "menu-close")
            {
                SetButtons(2);
            }
            else
            {
                SetButtons(3);
            }

            panelMenu.Visible = !panelMenu.Visible;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopConsuming();
            continueToReadFile = false;
            continueToReadTcp = false;
            Application.DoEvents();
        }

        private void BtnAbout_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.Show();
        }

        private void CbCMA_CheckedChanged(object sender, EventArgs e)
        {
            demodulator.CmaEnabled = cbCMA.Checked;
        }

        private async void ConsumeDemodulatedSymbols()
        {
            while (await decoderOutputQue.OutputAvailableAsync())
            {
                DemodulatedSymbolsArgs r = decoderOutputQue.Receive();
                Array.Reverse(r.Symbols);
                decoderInput.Post(r.Symbols);
            }
        }

        private async void ConsumeSignalAttributes()
        {
            while (await signalAttributesQue.OutputAvailableAsync())
            {
                SignalAttributesArgs r = signalAttributesQue.Receive();
                OnSignalAttributes(this, (SignalAttributesArgs)r);
            }
        }

        private async void ConsumeDescrambledFrames()
        {
            while (await decoderOutput.OutputAvailableAsync())
            {
                byte[] r = decoderOutput.Receive();
                DescrambledFrameArgs e = new DescrambledFrameArgs();
                e.DescrambledFrame = r;
                e.Length = r.Length;
                OnDescrambledFrame(this, e);
            }
        }

        private void CbAGC_CheckedChanged(object sender, EventArgs e)
        {
            demodulator.AgcEnabled = cbAGC.Checked;
        }

        private void NLoFreq_ValueChanged(object sender, EventArgs e)
        {
            //set lo frequency range of the demodulator
            demodulator?.SetLoFreq((int)nLoFreq.Value);
            fftD?.SetLoFreq((int)nLoFreq.Value);
        }

        private void NHiFreq_ValueChanged(object sender, EventArgs e)
        {
            //set hi frequency range of the demodulator
            demodulator?.SetHiFreq((int)nHiFreq.Value);
            fftD?.SetHiFreq((int)nHiFreq.Value);
        }

        /// <summary>
        /// Everywhere when we have combo boxes, we should go by the text, not by the index, as the device numbers 
        /// not always point to the same device. This is easy for now.
        /// TODO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Settings.Default.rAudioDevice = rAudioDevice.Checked;
            Settings.Default.comboPlaybackDevices = comboPlaybackDevices.SelectedIndex;
            Settings.Default.rFile = rFile.Checked;
            Settings.Default.txtWaveFileInput = txtWaveFileInput.Text;
            Settings.Default.openWavFileInput = openWavFileInput.FileName;
            Settings.Default.rTcpIp = rTcpIp.Checked;
            Settings.Default.txtTcpAddress = txtTcpAddress.Text;
            Settings.Default.nTcpPort = nTcpPort.Value;
            Settings.Default.txtUdpAddress = txtUdpAddress.Text;
            Settings.Default.cbCMA = cbCMA.Checked;
            Settings.Default.cbAGC = cbAGC.Checked;
            Settings.Default.nLoFreq = nLoFreq.Value;
            Settings.Default.nHiFreq = nHiFreq.Value;
            Settings.Default.nTolerance = nTolerance.Value;
            Settings.Default.nUdpPort = nUdpPort.Value;
            Settings.Default.cbUdpTransmit = cbUdpTransmit.Checked;
            Settings.Default.cbSuspendSleepMode = cbSuspendSleepMode.Checked;
            Settings.Default.nMaxDisplayedFrames = nMaxDisplayedFrames.Value;
            Settings.Default.comboPlaybackChannelIndex = comboPlaybackChannels.SelectedIndex;

            Settings.Default.Save();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = string.Format("v.{0}", FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly‌​().Location).Product‌​Version);
            try
            {
                rtbNotes.LoadFile(AppDomain.CurrentDomain.BaseDirectory + "notes.rtf", RichTextBoxStreamType.PlainText);
            }
            catch (Exception ex)
            {
                rtbNotes.Text = ex.Message;
            }
        }

        private void LostPacketTimer_Tick(object sender, EventArgs e)
        {
            //evaluate lost packets
            DateTime time = DateTime.UtcNow;
            if (time.CompareTo(stats.GetMarkTime()) == -1)
            {
                //the evaluation for lost packets is currently suspended
                return;
            }
            else
            {
                stats.EvaluateLostPackets(time, rFile.Checked);
            }
        }

        private void BtnPacketsAutoScroll_Click(object sender, EventArgs e)
        {
            (sender as ToolStripButton).Checked = !(sender as ToolStripButton).Checked;
        }

        private void ToolStripButton4_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string line in rtbFrames.Lines)
                sb.AppendLine(line);

            if (sb.Length > 0)
            {
                Clipboard.SetText(sb.ToString());
            }
        }

        private void BtnPacketsClear_Click(object sender, EventArgs e)
        {
            rtbFrames.Clear();
        }

        private void RSource_CheckedChanged(object sender, EventArgs e)
        {
            int selectedIndex = 0;
            selectedIndex = comboPlaybackChannels.SelectedIndex;
            comboPlaybackChannels.Items.Clear();
            comboPlaybackChannels.Items.Add("PCM 16-bit Mono");
            comboPlaybackChannels.Items.Add("PCM 16-bit Stereo Left");
            comboPlaybackChannels.Items.Add("PCM 16-bit Stereo Right");

            if (rAudioDevice.Checked)
            {
                comboPlaybackDevices.Enabled = true;
                txtWaveFileInput.Enabled = false;
                btnSelectWav.Enabled = false;
                txtTcpAddress.Enabled = false;
                nTcpPort.Enabled = false;

                selectedIndex = (selectedIndex == 3) ? 0 : selectedIndex;
                comboPlaybackChannels.SelectedIndex = selectedIndex;
            }
            else if (rFile.Checked)
            {
                comboPlaybackDevices.Enabled = false;
                txtWaveFileInput.Enabled = true;
                btnSelectWav.Enabled = true;
                txtTcpAddress.Enabled = false;
                nTcpPort.Enabled = false;

                selectedIndex = (selectedIndex == 3) ? 0 : selectedIndex;
                comboPlaybackChannels.SelectedIndex = selectedIndex;
            }
            else //rTcpIp
            {
                comboPlaybackDevices.Enabled = false;
                txtWaveFileInput.Enabled = false;
                btnSelectWav.Enabled = false;
                txtTcpAddress.Enabled = true;
                nTcpPort.Enabled = true;

                comboPlaybackChannels.Items.Add("PCM 16-bit IQ (Re Left, Im right)");
                comboPlaybackChannels.SelectedIndex = selectedIndex;
            }
        }

        private void CmaResetTimer_Tick(object sender, EventArgs e)
        {
            CmaResetTimer.Enabled = false;
            demodulator.CmaReset();
            Application.DoEvents();
            demodulator.CmaReset();
        }
    }

    internal class WavReaderState
    {
        WaveFileReader wfr = null;
        byte[] readArray = null;
        ManualResetEvent manualEvent = null;

        internal WaveFileReader FStream
        {
            get
            {
                return wfr;
            }
        }

        public byte[] ReadArray
        {
            get
            {
                return readArray;
            }
        }

        public ManualResetEvent ManualEvent
        {
            get
            {
                return manualEvent;
            }
        }
    }

    /// <summary>
    /// https://stackoverflow.com/questions/3063320/combobox-adding-text-and-value-to-an-item-no-binding-source, Adam Markowitz
    /// </summary>
    internal class ComboBoxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public ComboBoxItem (string text, object value)
        {
            Text = text;
            Value = value;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
